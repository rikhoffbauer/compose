import { writeFile } from "fs";
import mkdirp from "mkdirp";
import { dirname, join } from "path";

const createComposeOverload = (
    composeArgCount: number = 20,
    signatureArgumentCount: number = 5,
) => {
    const indent = `    `;
    const generics = [];
    const fns = [];
    const fnBaseName = `fn`;
    const argumentBaseName = `arg`;
    const returnValueGenericBaseName = `R`;
    const signatureArgumentGenericBaseName = `A`;
    let signatureArgs = [];

    for (let si = 1; si <= signatureArgumentCount; si++) {
        generics.push(`${signatureArgumentGenericBaseName}${si}`);
        signatureArgs.push(
            `${argumentBaseName}${si}: ${signatureArgumentGenericBaseName}${si}`,
        );
    }

    for (let fi = 1; fi <= composeArgCount; fi++) {
        const returnValue = `${returnValueGenericBaseName}${fi}`;
        const fnArgs =
            fi < composeArgCount
                ? [
                      `${argumentBaseName}: ${returnValueGenericBaseName}${fi +
                          1}`,
                  ]
                : signatureArgs;

        generics.push(returnValue);

        fns.push(
            `${fnBaseName}${fi}: (${fnArgs.join(", ")}) => ${returnValue}`,
        );
    }

    return `${indent}<${generics.join(", ")}>(${fns.join(
        ", ",
    )}): (${signatureArgs.join(", ")}) => ${returnValueGenericBaseName}1;`;
};

const createComposeInterface = (
    functionCount: number = 20,
    signatureArgumentCount = 5,
) => {
    const overloads = [];

    for (let funcCount = 1; funcCount <= functionCount; funcCount++) {
        for (
            let sigArgCount = 0;
            sigArgCount <= signatureArgumentCount;
            sigArgCount++
        ) {
            overloads.push(createComposeOverload(funcCount, sigArgCount));
        }
    }

    overloads.push(`    (...fns: Function[]): (...args: any[]) => any;`);

    return `export interface Compose {\n` + overloads.join("\n") + `\n}`;
};

const composeDst = join(__dirname, "../../generated/compose.ts");

mkdirp(dirname(composeDst), err => {
    if (err)
        return console.error(
            `Failed to create compose type definitions`,
            err.message,
        );

    writeFile(composeDst, createComposeInterface(), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create compose type definitions`,
                err.message,
            );

        console.log(`Successfully created compose type definitions`);
    });
});
