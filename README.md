# @rikhoffbauer/compose
![coverage](https://gitlab.com/rikhoffbauer/compose/badges/master/coverage.svg)
![build](https://gitlab.com/rikhoffbauer/compose/badges/master/build.svg)
![npm version](https://badge.fury.io/js/%40rikhoffbauer%2Fcompose.svg)

A compose function written in TypeScript.

## Installing

##### Using npm

`npm install @rikhoffbauer/compose`

##### Using yarn

`yarn add @rikhoffbauer/compose`

## Usage

```typescript
import compose from "@rikhoffbauer/compose";

const add = (right: number) => (left: number) => left + right;
const divide = (divisor: number) => (dividend: number) => dividend / divisor;
const add3AndDivideBy2 = compose(divide(2), add(3));

add3AndDivideBy2(3); // returns 3 ((3 + 3) / 2)
```

For more information see the [API documentation](https://rikhoffbauer.gitlab.io/compose/).

## Contributing

Contributions in the form of pull requests and bug reports are appreciated.

### Running tests

Tests are ran using the `test` npm script.
Both the functionality of the code at runtime and its types should be tested.

##### Using npm

`npm test`

##### Using yarn

`yarn test`
