import { Compose } from "../generated/compose";

// TODO move to separate npm package
const identity = <T>(value: T): T => value;

/**
 * `compose` composes the provided functions.
 * ### Simple example
 *  ```
 *  import compose from "@rikhoffbauer/compose";
 *  const add = (right: number) => (left: number) => left + right;
 *  const divide = (divisor: number) => (dividend: number) => dividend / divisor;
 *  const add3AndDivideBy2 = compose(divide(2), add(3));
 *  add3AndDivideBy2(3); // returns 3, because divide(2)(add(3)(3)) is 3
 *  ```
 */
const compose: Compose = (...fns: Function[]) => (...args: any[]) =>
    fns.reduceRight(
        (val, fn) => (typeof fn === "function" ? fn(val) : val),
        (fns.pop() || identity)(...args),
    );

export default compose;
