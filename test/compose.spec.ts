import { exec } from "child_process";
import compose from "../src";

const add = (right: number) => (left: number) => left + right;
const divide = (divisor: number) => (dividend: number) => dividend / divisor;

// TODO move to separate npm package
const identity = <T>(value: T): T => value;

describe(`@rikhoffbauer/compose`, () => {
    it(`should compose the provided functions`, async () => {
        const add3AndDivideBy2 = compose(divide(2), add(3));
        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    it(`should pass all (more than 1) arguments passed initially to the first executed (last argument) function`, async () => {
        const firstFn = jest.fn((...args: any[]) => args[0]);
        const secondFn: typeof identity = jest.fn(identity);
        const fn = compose(secondFn, firstFn);
        const args = [1, 2, 3];

        fn(...args);

        expect(firstFn).toHaveBeenCalledWith(...args);
        expect(secondFn).toHaveBeenCalledWith(args[0]);
    });

    it(`should ignore non function arguments`, async () => {
        // the any casts are necessary
        // because this kind of usage is prevented by the types as well
        const add3AndDivideBy2: any = compose(
            divide(2),
            null as any,
            "asdasd" as any,
            add(3),
            true as any,
            123 as any,
            undefined as any,
        );
        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    describe("types", () => {
        const testType = (path: string) =>
            new Promise((resolve, reject) =>
                exec(
                    `tsc --lib es2017 ${path}`,
                    err => (err ? reject(err) : resolve()),
                ),
            );

        it("should not throw compiler errors", async () => {
            await expect(
                testType(`./test/__fixtures__/correct-return-value.ts`),
            ).resolves.toBeUndefined();
        });
    });
});
