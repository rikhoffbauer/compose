const tsc = require('typescript');

const allowedExtensions = ["ts", "tsx"];

module.exports = {
    process (src, path) {
        const ext = path.split('.').pop();

        if (!allowedExtensions.includes(ext)) {
            return src;
        }

        const transpiled = tsc.transpileModule(src, {
            fileName       : path,
            compilerOptions: {
                sourceMap: true,
                jsx      : tsc.JsxEmit.React,
                module   : tsc.ModuleKind.CommonJS,
            }
        });

        return {
            code: transpiled.outputText,
            map : transpiled.sourceMapText,
        };
    },
};
