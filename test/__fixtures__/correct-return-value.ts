import compose from "../../src";

interface State {
    deeper: {
        deepest: string;
    };
}

const rootState: State = { deeper: { deepest: "" } };

const getDeeper = (state: State) => state.deeper;
const getDeepest = (state: State["deeper"]) => state.deepest;

const getDeepestFromRoot = compose(getDeepest, getDeeper);

const value: string = getDeepestFromRoot(rootState);
//const value: number = getDeepestFromRoot(rootState);
